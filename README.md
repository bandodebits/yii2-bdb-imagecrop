Bando de Bits Image Crop
========================
Bando de Bits Image Crop

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bandodebits/yii2-bdb-imagecrop "*"
```

or add

```
"bandodebits/yii2-bdb-imagecrop": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \bdb\imagecrop\AutoloadExample::widget(); ?>```